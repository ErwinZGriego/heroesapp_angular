# HeroesApp_Angular

TypeScript application using angular as the framework. It is a webapp that shows a list of DC or Marvel Superheros, when a hero is clicked it takes you to a new view where you can see more info on that superhero. It has a search funcitonality to look for a specific superhero. It shall have a CRUD funcitonality to view, add, edit, or remove information. It has an info pipeline to a local json database where it gets all the hero info. It also has various routes in place to locate that information.
